# Loss functions

import torch
import torch.nn as nn
import numpy as np
from utils.general import bbox_iou
from utils.torch_utils import is_parallel

from Module_Store import *
# # wst_gain for compute loss
# wst_gain_1 = 0.5
# wst_gain_2 = 0.5
# wst_loss = 'v3'
# '''
# 无所谓 WST Head 是否单独，包含：v1, v3
# 说明：
#     v1：关键点加权差值方法, 直接差值也在这里, loss_module_name = 'v1' 或 'v1_Cosdis'
#     v3：类别均值向量全局差值方法, loss_module_name = 'v3' 或 'v3_Cosdis'
# '''
# loss_module_name = 'v3'
# '''
# 当 WST Head 单独时，包含：v1, v1_cosdis, v3, v3_cosdis
# 说明：
#     v1: L1, L2, Cosine of v1
#     v1_cosdis: Cosdis of v1
#     v3: L1, L2, Cosine of v3
#     v3_cosdis: Cosdis of v3
# 当 WST Head 与 yolo Head 同一时，包含：v1, v3
# 说明：
#     v1: L1, L2, Cosine of v1
#     v3: L1, L2, Cosine of v3
# '''
#
# measure = 'Cosine'
# '''
# 无所谓 WST Head 是否单独，包含：L1, L2, Cosine
# 说明：
#     当 loss_module_name = 'v1_cosdis' 或 'v3_cosdis' 时，measure随意，因为不起作用
# '''
#
# BN_WST_module = 'None'
# '''
# 包含：Done, None
# 说明：
#     Done: 在做度量前 Feature maps 做 BN 操作
#     None: 在做度量前 Feature maps 不做 BN 操作
# '''
#
# OHEM_module = 'None'
# '''
# 包含：Done, None
# 说明：
#     Done: 在做Loss前做 OHEM 难例硬筛选操作
#     None: 在做Loss前不做 OHEM 难例硬筛选操作
# '''
#
# multi_task_module = 'Done'


def smooth_BCE(eps=0.1):  # https://github.com/ultralytics/yolov3/issues/238#issuecomment-598028441
    # return positive, negative label smoothing BCE targets
    return 1.0 - 0.5 * eps, 0.5 * eps


class BCEBlurWithLogitsLoss(nn.Module):
    # BCEwithLogitLoss() with reduced missing label effects.
    def __init__(self, alpha=0.05):
        super(BCEBlurWithLogitsLoss, self).__init__()
        self.loss_fcn = nn.BCEWithLogitsLoss(reduction='none')  # must be nn.BCEWithLogitsLoss()
        self.alpha = alpha

    def forward(self, pred, true):
        loss = self.loss_fcn(pred, true)
        pred = torch.sigmoid(pred)  # prob from logits
        dx = pred - true  # reduce only missing label effects
        # dx = (pred - true).abs()  # reduce missing label and false label effects
        alpha_factor = 1 - torch.exp((dx - 1) / (self.alpha + 1e-4))
        loss *= alpha_factor
        return loss.mean()


class FocalLoss(nn.Module):
    # Wraps focal loss around existing loss_fcn(), i.e. criteria = FocalLoss(nn.BCEWithLogitsLoss(), gamma=1.5)
    def __init__(self, loss_fcn, gamma=1.5, alpha=0.25):
        super(FocalLoss, self).__init__()
        self.loss_fcn = loss_fcn  # must be nn.BCEWithLogitsLoss()
        self.gamma = gamma
        self.alpha = alpha
        self.reduction = loss_fcn.reduction
        self.loss_fcn.reduction = 'none'  # required to apply FL to each element

    def forward(self, pred, true):
        loss = self.loss_fcn(pred, true)
        # p_t = torch.exp(-loss)
        # loss *= self.alpha * (1.000001 - p_t) ** self.gamma  # non-zero power for gradient stability

        # TF implementation https://github.com/tensorflow/addons/blob/v0.7.1/tensorflow_addons/losses/focal_loss.py
        pred_prob = torch.sigmoid(pred)  # prob from logits
        p_t = true * pred_prob + (1 - true) * (1 - pred_prob)
        alpha_factor = true * self.alpha + (1 - true) * (1 - self.alpha)
        modulating_factor = (1.0 - p_t) ** self.gamma
        loss *= alpha_factor * modulating_factor

        if self.reduction == 'mean':
            return loss.mean()
        elif self.reduction == 'sum':
            return loss.sum()
        else:  # 'none'
            return loss


class QFocalLoss(nn.Module):
    # Wraps Quality focal loss around existing loss_fcn(), i.e. criteria = FocalLoss(nn.BCEWithLogitsLoss(), gamma=1.5)
    def __init__(self, loss_fcn, gamma=1.5, alpha=0.25):
        super(QFocalLoss, self).__init__()
        self.loss_fcn = loss_fcn  # must be nn.BCEWithLogitsLoss()
        self.gamma = gamma
        self.alpha = alpha
        self.reduction = loss_fcn.reduction
        self.loss_fcn.reduction = 'none'  # required to apply FL to each element

    def forward(self, pred, true):
        loss = self.loss_fcn(pred, true)

        pred_prob = torch.sigmoid(pred)  # prob from logits
        alpha_factor = true * self.alpha + (1 - true) * (1 - self.alpha)
        modulating_factor = torch.abs(true - pred_prob) ** self.gamma
        loss *= alpha_factor * modulating_factor

        if self.reduction == 'mean':
            return loss.mean()
        elif self.reduction == 'sum':
            return loss.sum()
        else:  # 'none'
            return loss


class ComputeLoss:
    # Compute losses
    def __init__(self, model, autobalance=False):
        super(ComputeLoss, self).__init__()
        device = next(model.parameters()).device  # get model device
        h = model.hyp  # hyperparameters

        # Define criteria
        BCEcls = nn.BCEWithLogitsLoss(pos_weight=torch.tensor([h['cls_pw']], device=device))
        BCEobj = nn.BCEWithLogitsLoss(pos_weight=torch.tensor([h['obj_pw']], device=device))

        # Class label smoothing https://arxiv.org/pdf/1902.04103.pdf eqn 3
        self.cp, self.cn = smooth_BCE(eps=h.get('label_smoothing', 0.0))  # positive, negative BCE targets

        # Focal loss
        g = h['fl_gamma']  # focal loss gamma
        if g > 0:
            BCEcls, BCEobj = FocalLoss(BCEcls, g), FocalLoss(BCEobj, g)

        det = model.module.model[-1] if is_parallel(model) else model.model[-1]  # Detect() module

        self.balance = {3: [4.0, 1.0, 0.4]}.get(det.nl, [4.0, 1.0, 0.25, 0.06, .02])  # P3-P7
        self.ssi = list(det.stride).index(16) if autobalance else 0  # stride 16 index
        self.BCEcls, self.BCEobj, self.gr, self.hyp, self.autobalance = BCEcls, BCEobj, model.gr, h, autobalance
        for k in 'na', 'nc', 'nl', 'anchors':
            setattr(self, k, getattr(det, k))

    def __call__(self, p, targets):  # predictions, targets, model
        device = targets.device
        lcls, lbox, lobj = torch.zeros(1, device=device), torch.zeros(1, device=device), torch.zeros(1, device=device)
        tcls, tbox, indices, anchors = self.build_targets(p, targets)  # targets

        # Losses
        for i, pi in enumerate(p):  # layer index, layer predictions
            b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx
            tobj = torch.zeros_like(pi[..., 0], device=device)  # target obj

            n = b.shape[0]  # number of targets
            if n:
                ps = pi[b, a, gj, gi]  # prediction subset corresponding to targets

                # Regression
                pxy = ps[:, :2].sigmoid() * 2. - 0.5
                pwh = (ps[:, 2:4].sigmoid() * 2) ** 2 * anchors[i]
                pbox = torch.cat((pxy, pwh), 1)  # predicted box
                iou = bbox_iou(pbox.T, tbox[i], x1y1x2y2=False, CIoU=True)  # iou(prediction, target)
                lbox += (1.0 - iou).mean()  # iou loss

                # Objectness
                tobj[b, a, gj, gi] = (1.0 - self.gr) + self.gr * iou.detach().clamp(0).type(tobj.dtype)  # iou ratio

                # Classification
                if self.nc > 1:  # cls loss (only if multiple classes)
                    t = torch.full_like(ps[:, 5:], self.cn, device=device)  # targets
                    t[range(n), tcls[i]] = self.cp
                    lcls += self.BCEcls(ps[:, 5:], t)  # BCE

                # Append targets to text file
                # with open('targets.txt', 'a') as file:
                #     [file.write('%11.5g ' * 4 % tuple(x) + '\n') for x in torch.cat((txy[i], twh[i]), 1)]

            obji = self.BCEobj(pi[..., 4], tobj)
            lobj += obji * self.balance[i]  # obj loss
            if self.autobalance:
                self.balance[i] = self.balance[i] * 0.9999 + 0.0001 / obji.detach().item()

        if self.autobalance:
            self.balance = [x / self.balance[self.ssi] for x in self.balance]
        lbox *= self.hyp['box']
        lobj *= self.hyp['obj']
        lcls *= self.hyp['cls']
        bs = tobj.shape[0]  # batch size

        loss = lbox + lobj + lcls
        return loss * bs, torch.cat((lbox, lobj, lcls, loss)).detach()

    def build_targets(self, p, targets):
        # Build targets for compute_loss(), input targets(image,class,x,y,w,h)
        na, nt = self.na, targets.shape[0]  # number of anchors, targets
        tcls, tbox, indices, anch = [], [], [], []
        gain = torch.ones(7, device=targets.device)  # normalized to gridspace gain
        ai = torch.arange(na, device=targets.device).float().view(na, 1).repeat(1, nt)  # same as .repeat_interleave(nt)
        targets = torch.cat((targets.repeat(na, 1, 1), ai[:, :, None]), 2)  # append anchor indices

        g = 0.5  # bias
        off = torch.tensor([[0, 0],
                            [1, 0], [0, 1], [-1, 0], [0, -1],  # j,k,l,m
                            # [1, 1], [1, -1], [-1, 1], [-1, -1],  # jk,jm,lk,lm
                            ], device=targets.device).float() * g  # offsets

        for i in range(self.nl):
            anchors = self.anchors[i]
            gain[2:6] = torch.tensor(p[i].shape)[[3, 2, 3, 2]]  # xyxy gain

            # Match targets to anchors
            t = targets * gain
            if nt:
                # Matches
                r = t[:, :, 4:6] / anchors[:, None]  # wh ratio
                j = torch.max(r, 1. / r).max(2)[0] < self.hyp['anchor_t']  # compare
                # j = wh_iou(anchors, t[:, 4:6]) > model.hyp['iou_t']  # iou(3,n)=wh_iou(anchors(3,2), gwh(n,2))
                t = t[j]  # filter

                # Offsets
                gxy = t[:, 2:4]  # grid xy
                gxi = gain[[2, 3]] - gxy  # inverse
                j, k = ((gxy % 1. < g) & (gxy > 1.)).T
                l, m = ((gxi % 1. < g) & (gxi > 1.)).T
                j = torch.stack((torch.ones_like(j), j, k, l, m))
                t = t.repeat((5, 1, 1))[j]
                offsets = (torch.zeros_like(gxy)[None] + off[:, None])[j]
            else:
                t = targets[0]
                offsets = 0

            # Define
            b, c = t[:, :2].long().T  # image, class
            gxy = t[:, 2:4]  # grid xy
            gwh = t[:, 4:6]  # grid wh
            gij = (gxy - offsets).long()
            gi, gj = gij.T  # grid xy indices

            # Append
            a = t[:, 6].long()  # anchor indices
            indices.append((b, a, gj.clamp_(0, gain[3] - 1), gi.clamp_(0, gain[2] - 1)))  # image, anchor, grid indices
            tbox.append(torch.cat((gxy - gij, gwh), 1))  # box
            anch.append(anchors[a])  # anchors
            tcls.append(c)  # class

        return tcls, tbox, indices, anch


''' ========================= Wst 对比学习 ===================== '''
'''类别均值向量全局差值 loss模块——WST Head'''
class RoyFocalLoss_v3_WH(nn.Module):
    # Wraps focal loss around existing loss_fcn(), i.e. criteria = FocalLoss(nn.BCEWithLogitsLoss(), gamma=1.5)
    def __init__(self, BN_WST, gamma=2.0, alpha=0.25, omg=0.1, reduction='sum'):
        super(RoyFocalLoss_v3_WH, self).__init__()
        # self.gamma = torch.tensor(gamma, device='cuda:0')
        self.gamma = gamma
        # self.alpha = torch.tensor(alpha, device='cuda:0')
        self.alpha = alpha
        # self.omg = torch.tensor(omg, device='cuda:0')
        self.omg = omg
        self.reduction = reduction
        self.BN_WST = BN_WST

    def batch_norm_v3(self, x, gamma, beta, moving_means, moving_vars, moving_momentum, is_train=True, bn_dim=0):
        eps = 1e-5
        x_mean = torch.mean(x, dim=bn_dim)
        x_var = torch.sqrt(torch.mean((x - x_mean) ** 2, dim=bn_dim))
        if is_train == True:
            moving_means = moving_momentum * moving_means + (1. - moving_momentum) * x_mean
            moving_vars = moving_momentum * moving_vars + (1. - moving_momentum) * x_var
            x_hat = (x - x_mean) / (x_var + eps)
            return gamma * x_hat + beta, moving_means.detach(), moving_vars.detach()
        else:
            x_mean = (1. - moving_momentum) * moving_means + moving_momentum * x_mean
            x_var = (1. - moving_momentum) * moving_vars + moving_momentum * x_var
            x_hat = (x - x_mean) / (x_var + eps)
            return gamma * x_hat + beta, moving_means, moving_vars

    def cf_v3(eslf, x, c_means, moving_momentum, is_train=True, c_dim=0):
        x_mean = torch.mean(x, dim=c_dim)
        c_means = moving_momentum * c_means + (1. - moving_momentum) * x_mean
        if is_train == True:
            return c_means, c_means.detach()
        else:
            return c_means, c_means

    # ===> (-1, 0)
    def bn_linear(self, x, x_max, x_min):
        x = x - x_max - 0.001
        x = torch.div(x, (0.002 + (x_max + 0.001 - x_min)))    # (-1, 0)
        return x

    # ===> (-1, 1)
    def compute_cosine_L2_v3(self, ps_1, ps_2):
        '''～～～～～～～～～～～～～～～～～ Cosine 版本 ～～～～～～～～～～～～～～～～～'''
        wst_d_a = ps_1.unsqueeze(dim=1)
        wst_d_b = ps_2.unsqueeze(dim=0)
        wst_d_temp = wst_d_a * wst_d_b
        wst_d_temp = wst_d_temp.sum(dim=2)

        wst_l1_a = torch.norm(ps_1, dim=1, p=2)
        wst_l1_a = wst_l1_a.unsqueeze(dim=1)
        wst_l1_b = torch.norm(ps_2, dim=1, p=2)
        wst_l1_b = wst_l1_b.unsqueeze(dim=0)
        wst_l1_temp = wst_l1_a * wst_l1_b

        wst_temp = wst_d_temp / wst_l1_temp   #(-1, 1)
        wst_temp = wst_temp - 0.00001
        temp_wst_temp = torch.max(wst_temp)
        '''～～～～～～～～～～～～～～～～～ Cosine 版本 -- end ～～～～～～～～～～～～～～～～～'''
        if temp_wst_temp > 1.0:
            print("\n\033[1;33m temp_wst_temp ===> ", temp_wst_temp)
            print("wst_d_temp ===> ", wst_d_temp)
            print("wst_l1_temp ===> ", wst_l1_temp)
            print("\033[0m")
        return wst_temp

    def forward(self, cwg, pred, true, epoch, epochs, is_train,
                cf_means, cf_move, fe_means, fe_vars, fe_move, measure_loss='L1'):
        '''========================== Meta Learning ================================='''
        nb = pred.shape[0]
        _, k_t_id = torch.max(true, dim=1)  # find the class of every feature vec
        ps = pred.clone()

        '''============ 特征向量预处理 BN操作 ============'''
        gamma_bn = torch.ones(1, device='cuda:0')
        beta_bn = torch.zeros(1, device='cuda:0')
        # 各特征维度做BN，在单一特征维度上保持正态分布
        m_m = torch.tensor((10.0 * epoch / epochs))
        m_mfe = torch.sigmoid(m_m) * fe_move
        if BN_WST_module == 'Done':
            ps[:, 5:], fe_means[epoch, :], fe_vars[epoch, :] = \
                self.BN_WST(ps[:, 5:], fe_means[epoch, :], fe_vars[epoch, :], m_mfe, is_train=is_train)

        new_ps = ps[:, 5:].clone()
        new_ps = torch.cat((k_t_id[:, None], new_ps), 1)  # feature vec里在第一个元素上加入类别标签

        '''====================== 基于类别，统计各类feature vecs的均值向量 ======================'''
        '''ct：统计当前ps序列中所有出现的类别'''
        ct_idx = true.sum(dim=0)
        ct_idx = ct_idx.cpu().numpy()
        ct = np.argwhere(ct_idx != 0)
        ct = torch.from_numpy(ct)
        ct = ct.to(new_ps.device)

        '''
        idx_ci：当前ps序列中属于i类的行索引
        ps_ci：当前ps序列中属于i类的所以feature vecs的集合
        m_ps_ci：当前ps序列中属于i类的所以feature vecs的均值向量
        '''
        new_ps_n = new_ps.detach().cpu().numpy()  # 便于操作

        m_mcf = torch.sigmoid(m_m) * cf_move
        names = locals()  # 便于动态命名，使其变量名能与类别标签对应上
        for ci in range(cf_means.shape[1]):
            names['idx_c%s' % ci] = np.argwhere(new_ps_n[:, 0] == ci)
            if names['idx_c%s' % ci].size != 0:
                names['idx_c%s' % ci] = torch.from_numpy(names['idx_c%s' % ci]).to(new_ps.device)
                names['ps_c%s' % ci] = new_ps[names['idx_c%s' % ci], 1:]
                names['ps_c%s' % ci] = names['ps_c%s' % ci].squeeze(1)
                # names['m_ps_c%s' % ci] = torch.mean(names['ps_c%s' % ci], dim=0)
                names['m_ps_c%s' % ci], cf_means[epoch, ci, :] = \
                    self.cf_v3(names['ps_c%s' % ci], cf_means[epoch, ci, :], m_mcf, is_train=is_train)

        '''m_ps_cs：当前ps序列中所有出现的类别的均值向量集合'''
        for ci in range(ct.shape[0]):
            temp_ci = int(ct[ci, 0])
            if ci == 0:
                m_ps_cs = names['m_ps_c%s' % temp_ci].unsqueeze(0)
            else:
                m_ps_cs = torch.cat((m_ps_cs, names['m_ps_c%s' % temp_ci].unsqueeze(0)), 0)

        '''====================== 计算差值 ======================'''
        ''' 
        rd_cs_ps：将所有rd_ci按照类别号依次拼接为ps.shape[0]大小的集合
        idx_ct_ps：rd_cs_ps 中元素对应的feature vecs在ps中的行索引
        '''
        for ci in range(ct.shape[0]):
            temp_ci = int(ct[ci, 0])
            if measure_loss == 'L1':
                names['rd_c%s' % temp_ci] = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci]).abs().sum(dim=1)
            elif measure_loss == 'L2':
                temp_aaa_subdis = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci])
                names['rd_c%s' % temp_ci] = torch.norm(temp_aaa_subdis, dim=1, p=2).unsqueeze(dim=1)
            elif measure_loss == 'Cosine':
                temp_aaa = self.compute_cosine_L2_v3(names['ps_c%s' % temp_ci],
                                                  names['m_ps_c%s' % temp_ci].unsqueeze(dim=0))    # (-1, 1)
                names['rd_c%s' % temp_ci] = (1.0 - temp_aaa).squeeze(dim=1)    # (0, 2)

                if torch.max(temp_aaa) > 1.0:
                    print("\n\033[1;33m training: ")
                    print("temp_aaa ===> ", temp_aaa)
                    print("temp_aaa ===> %.20f" % torch.max(temp_aaa))
                    print("\033[0m")

            if ci == 0:
                rd_cs_ps = names['rd_c%s' % temp_ci]
                temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                idx_ct_ps = temp_idx
            else:
                rd_cs_ps = torch.cat((rd_cs_ps, names['rd_c%s' % temp_ci]), 0)
                temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                idx_ct_ps = torch.cat((idx_ct_ps, temp_idx), 0)

        ''' 
        rd_all：ps中所有feature vecs与ps中所有出现过的类别的均值的差值（L1） 
        '''
        if measure_loss == 'L1':
            rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=1)
        elif measure_loss == 'L2':
            rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=2)
        elif measure_loss == 'Cosine':
            temp_bbb = self.compute_cosine_L2_v3(ps[:, 5:], m_ps_cs)    # (-1, 1)
            rd_all = 1.0 - temp_bbb    # (0, 2)


        ''' 
        rd_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将ps重组 
        rt_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将t重组 
        '''
        idx_ct_ps = idx_ct_ps.to(torch.int64)
        rd_all_ct_ps = torch.index_select(rd_all, 0, idx_ct_ps)
        rt_all_ct_ps = torch.index_select(true, 0, idx_ct_ps)

        # cls_wst_gain
        if multi_task_module == 'Done':
            cwg_ct_ps = torch.index_select(cwg, 0, idx_ct_ps)
            cwg_ct_ps = cwg_ct_ps.unsqueeze(1)


        ''' 
        rd_cs_ps_d1：重组的ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1） 
        rd_cs_ps_d0：重组的ps中属于i类别的feature vecs与i类别的均值的差值（L1） 
        '''
        rd_cs_ps_d0 = rd_cs_ps[None, :].t()    # (0, 2)
        temp_0_min = torch.min(rd_cs_ps_d0)
        temp_0_max = torch.max(rd_cs_ps_d0)
        rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0)    # (-2, 2)

        if temp_0_min < 0.0:
            print("training \n rd_cs_ps ===> ", rd_cs_ps)

        if ct.shape[0] != 1:
            ''' rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 '''
            for ci in range(ct.shape[0]):
                temp_ci = int(ct[ci, 0])
                if ci == 0:
                    ci_idx = ci
                else:
                    ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
                names['rd_cs_ps_d1_c%s' % temp_ci] = rd_cs_ps_d1[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
                names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
                names['rd_cs_ps_d1_c%s' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci][:,
                                                     torch.arange(names['rd_cs_ps_d1_c%s' % temp_ci].size(1)) != ci]

                # cls_wst_gain
                if multi_task_module == 'Done':
                    names['cwg_c%s' % temp_ci] = cwg_ct_ps[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]

                if ci == 0:
                    temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci])
                    temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci])
                else:
                    temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.max(
                        names['rd_cs_ps_d1_c%s' % temp_ci]) > temp_1_max else temp_1_max
                    temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.min(
                        names['rd_cs_ps_d1_c%s' % temp_ci]) < temp_1_min else temp_1_min

            ''' rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid '''
            for ci in range(ct.shape[0]):
                temp_ci = int(ct[ci, 0])
                if measure_loss == 'L1' or measure_loss == 'L2':
                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = self.bn_linear(names['rd_cs_ps_d1_c%s' % temp_ci], temp_1_max, temp_1_min)   # (-1, 0)
                    # names['rd_cs_ps_d1_c%s_p' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci] - temp_1_max - 0.001
                    # names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d1_c%s_p' % temp_ci],
                    #                                                  (0.002 + (temp_1_max + 0.001 - temp_1_min)))   # (-1, 0)
                    # temp_1_mid = torch.div(10, (0.002 + (temp_1_max + 0.001 - temp_1_min)))
                    temp_1_mid = 0.5
                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + temp_1_mid)   # (-6, 6)

                elif measure_loss == 'Cosine':
                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = 3.0 * names['rd_cs_ps_d1_c%s' % temp_ci]   # (-6, 6)

                # cls_wst_gain
                if multi_task_module == 'Done':
                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = (names['rd_cs_ps_d1_c%s_p' % temp_ci] - 6.0) * names['cwg_c%s' % temp_ci] + 6.0

                names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d1_c%s_p' % temp_ci])


                if measure_loss == 'L1' or measure_loss == 'L2':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = self.bn_linear(names['rd_cs_ps_d0_c%s' % temp_ci], temp_0_max, temp_0_min)   # (-1, 0)
                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] - temp_0_max - 0.001
                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci],
                    #                                                  (0.002 + (temp_0_max + 0.001 - temp_0_min)))   # (-1, 0)
                    # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
                    temp_0_mid = 0.5
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] + temp_0_mid)   # (-6, 6)

                elif measure_loss == 'Cosine':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 6.0 * (names['rd_cs_ps_d0_c%s' % temp_ci] - 1.0)   # (-6, 6)

                # cls_wst_gain
                if multi_task_module == 'Done':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = (names['rd_cs_ps_d0_c%s_p' % temp_ci] + 6.0) * names['cwg_c%s' % temp_ci] - 6.0

                names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])


                if ci == 0:
                    rd_cs_ps_d1_ct_p = names['rd_cs_ps_d1_c%s_p' % temp_ci]
                    rd_cs_ps_d0_ct_p = names['rd_cs_ps_d0_c%s_p' % temp_ci]
                else:
                    rd_cs_ps_d1_ct_p = torch.cat((rd_cs_ps_d1_ct_p, names['rd_cs_ps_d1_c%s_p' % temp_ci]), 0)
                    rd_cs_ps_d0_ct_p = torch.cat((rd_cs_ps_d0_ct_p, names['rd_cs_ps_d0_c%s_p' % temp_ci]), 0)

            # print('\n rd_cs_ps_d1_ct_p \n', rd_cs_ps_d1_ct_p)
            # print('\n rd_cs_ps_d0_ct_p \n', rd_cs_ps_d0_ct_p)

            ''' BCELoss Loss '''
            loss_y1 = - torch.log(rd_cs_ps_d1_ct_p)
            loss_y0 = - torch.log(1 - rd_cs_ps_d0_ct_p)
            ''' Focal Loss '''
            # loss_y1 = - (1 - rd_cs_ps_d1_ct_p) ** self.gamma * self.alpha * torch.log(rd_cs_ps_d1_ct_p)
            # loss_y0 = - (rd_cs_ps_d0_ct_p) ** self.gamma * (1 - self.alpha) * torch.log(1 - rd_cs_ps_d0_ct_p)
            # loss_y1 = - (1 - rd_cs_ps_d1_ct_p) ** self.gamma * self.alpha * torch.log(rd_cs_ps_d1_ct_p)
            # loss_y0 = - (rd_cs_ps_d0_ct_p) ** self.gamma * (1 - self.alpha) * torch.log(1 - rd_cs_ps_d0_ct_p) - \
            #           (1 - rd_cs_ps_d0_ct_p) ** self.gamma * self.omg * torch.log(rd_cs_ps_d0_ct_p)
            loss = loss_y0 + loss_y1


        else:
            ''' rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 '''
            for ci in range(ct.shape[0]):
                temp_ci = int(ct[ci, 0])
                if ci == 0:
                    ci_idx = ci
                else:
                    ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
                names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]

                # cls_wst_gain
                if multi_task_module == 'Done':
                    names['cwg_c%s' % temp_ci] = cwg_ct_ps[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]

            ''' rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid '''
            for ci in range(ct.shape[0]):
                temp_ci = int(ct[ci, 0])
                if measure_loss == 'L1' or measure_loss == 'L2':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = self.bn_linear(names['rd_cs_ps_d0_c%s' % temp_ci], temp_0_max, temp_0_min)   # (-1, 0)
                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] - temp_0_max - 0.001
                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci],
                    #                                                  (0.002 + (temp_0_max + 0.001 - temp_0_min)))   # (-1, 0)

                    # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
                    temp_0_mid = 0.5
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] + temp_0_mid)   # (-6, 6)

                elif measure_loss == 'Cosine':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 6.0 * (names['rd_cs_ps_d0_c%s' % temp_ci] - 1.0)   # (-6, 6)

                # cls_wst_gain
                if multi_task_module == 'Done':
                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = (names['rd_cs_ps_d0_c%s_p' % temp_ci] + 6.0) * names['cwg_c%s' % temp_ci] - 6.0

                names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])

                if ci == 0:
                    rd_cs_ps_d0_ct_p = names['rd_cs_ps_d0_c%s_p' % temp_ci]
                else:
                    rd_cs_ps_d0_ct_p = torch.cat((rd_cs_ps_d0_ct_p, names['rd_cs_ps_d0_c%s_p' % temp_ci]), 0)

            # print('\n rd_cs_ps_d0_ct_p \n', rd_cs_ps_d0_ct_p)

            ''' BCELoss Loss '''
            loss_y0 = - torch.log(1 - rd_cs_ps_d0_ct_p)
            ''' Focal Loss '''
            # loss_y0 = - (rd_cs_ps_d0_ct_p) ** self.gamma * (1 - self.alpha) * torch.log(1 - rd_cs_ps_d0_ct_p)
            # loss_y0 = - (rd_cs_ps_d0_ct_p) ** self.gamma * (1 - self.alpha) * torch.log(1 - rd_cs_ps_d0_ct_p) - \
            #           (1 - rd_cs_ps_d0_ct_p) ** self.gamma * self.omg * torch.log(rd_cs_ps_d0_ct_p)
            loss = loss_y0


        nl = loss.shape[0]
        ii = loss.shape[1]

        if torch.isnan(loss.sum()):
            print('loss_sum = %s ' % (loss.sum()))
            print('loss_sum_y0 = %s ' % (loss_y0.sum()))
            print('loss_sum_y1 = %s ' % (loss_y1.sum()))

        if self.reduction == 'elementwise_mean':
            return loss.mean(), nl
        elif self.reduction == 'sum':
            return loss.sum(), nl, ii
        else:  # 'none'
            return loss, nl

class ComputeLoss_Wst:
    # Compute losses
    def __init__(self, model, autobalance=False):
        super(ComputeLoss_Wst, self).__init__()
        device = next(model.parameters()).device  # get model device
        h = model.hyp  # hyperparameters

        # Define criteria
        BCEcls = nn.BCEWithLogitsLoss(pos_weight=torch.tensor([h['cls_pw']], device=device))
        BCEobj = nn.BCEWithLogitsLoss(pos_weight=torch.tensor([h['obj_pw']], device=device))

        # Class label smoothing https://arxiv.org/pdf/1902.04103.pdf eqn 3
        self.cp, self.cn = smooth_BCE(eps=h.get('label_smoothing', 0.0))  # positive, negative BCE targets

        # Focal loss
        g = h['fl_gamma']  # focal loss gamma
        if g > 0:
            BCEcls, BCEobj = FocalLoss(BCEcls, g), FocalLoss(BCEobj, g)

        det = model.module.model[-1] if is_parallel(model) else model.model[-1]  # Detect() module


        ''' ========================= Wst 对比学习 ===================== '''
        if loss_module_name == 'v3':
            BN_WST = 1.0  # 没啥具体意义，用来占位
            BCEwst_v3_WH = RoyFocalLoss_v3_WH(BN_WST)  # v3
        det_wst = model.module.model[-2] if is_parallel(model) else model.model[-2]  # Wsthead() module


        self.balance = {3: [4.0, 1.0, 0.4]}.get(det.nl, [4.0, 1.0, 0.25, 0.06, .02])  # P3-P7
        self.ssi = list(det.stride).index(16) if autobalance else 0  # stride 16 index
        # self.BCEcls, self.BCEobj, self.gr, self.hyp, self.autobalance = BCEcls, BCEobj, model.gr, h, autobalance
        ''' ========================= Wst 对比学习 ===================== '''
        self.BCEcls, self.BCEobj, self.BCEwst_v3_WH, self.gr, self.hyp, self.autobalance = \
            BCEcls, BCEobj, BCEwst_v3_WH, model.gr, h, autobalance


        for k in 'na', 'nc', 'nl', 'anchors':
            setattr(self, k, getattr(det, k))

    def __call__(self, wst_p, p, targets, model, epoch, epochs, cf_means, cf_move,
                fe_means, fe_vars, fe_move, is_train=True):  # predictions, targets, model
        device = targets.device
        # lcls, lbox, lobj = torch.zeros(1, device=device), torch.zeros(1, device=device), torch.zeros(1, device=device)
        ''' ========================= Wst 对比学习 ===================== '''
        lcls, lbox, lobj, lwst = torch.zeros(1, device=device), torch.zeros(1, device=device), \
                                 torch.zeros(1, device=device), torch.zeros(1, device=device)


        tcls, tbox, indices, anchors = self.build_targets(p, targets)  # targets

        # Losses
        nt = 0  # targets
        for i, (pi, wst_pi) in enumerate(zip(p, wst_p)):  # layer index, layer predictions
            b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx
            tobj = torch.zeros_like(pi[..., 0], device=device)  # target obj

            n = b.shape[0]  # number of targets
            if n:
                ps = pi[b, a, gj, gi]  # prediction subset corresponding to targets


                ''' ========================= Wst 对比学习 ===================== '''
                nt += n  # cumulative targets
                wst_ps = wst_pi[b, a, gj, gi]  # prediction subset corresponding to targets


                # Regression
                pxy = ps[:, :2].sigmoid() * 2. - 0.5
                pwh = (ps[:, 2:4].sigmoid() * 2) ** 2 * anchors[i]
                pbox = torch.cat((pxy, pwh), 1)  # predicted box
                iou = bbox_iou(pbox.T, tbox[i], x1y1x2y2=False, CIoU=True)  # iou(prediction, target)
                lbox += (1.0 - iou).mean()  # iou loss

                # Objectness
                tobj[b, a, gj, gi] = (1.0 - self.gr) + self.gr * iou.detach().clamp(0).type(tobj.dtype)  # iou ratio

                # Classification
                if self.nc > 1:  # cls loss (only if multiple classes)
                    t = torch.full_like(ps[:, 5:], self.cn, device=device)  # targets
                    t[range(n), tcls[i]] = self.cp
                    lcls += self.BCEcls(ps[:, 5:], t)  # BCE


                    ''' ========================= Wst 对比学习 ===================== '''
                    wst_t = torch.full_like(wst_ps[:, :9], self.cn, device=device)  # targets
                    wst_t[range(n), tcls[i]] = self.cp


                # Append targets to text file
                # with open('targets.txt', 'a') as file:
                #     [file.write('%11.5g ' * 4 % tuple(x) + '\n') for x in torch.cat((txy[i], twh[i]), 1)]

            obji = self.BCEobj(pi[..., 4], tobj)
            lobj += obji * self.balance[i]  # obj loss
            if self.autobalance:
                self.balance[i] = self.balance[i] * 0.9999 + 0.0001 / obji.detach().item()


            ''' ========================= Wst 对比学习 ===================== '''
            if n > 1 and epoch <= 500:
                # t_wst_s = torch_utils.time_synchronized()
                # cls_wst_gain
                if multi_task_module == 'Done':
                    check_ps = ps[:, 5:].detach().sigmoid()  # sigmoid
                    check_t = t.detach()
                    _, ct = torch.max(check_t, dim=1)
                    _, cps = torch.max(check_ps, dim=1)
                    ct = ct.to(float)
                    cps = cps.to(float)
                    # global cls_wst_gain
                    cls_wst_gain = ct - cps
                    ones_cwg = torch.ones_like(cls_wst_gain)
                    others_cwg = 0.85 * ones_cwg
                    cls_wst_gain = torch.where(cls_wst_gain == 0.0, others_cwg, ones_cwg)
                else:
                    cls_wst_gain = 1.0

                if wst_loss == 'v3':
                    '''-------- v3 类别均值向量全局差值方法 wst_cmgl--------'''
                    loss_wst, nl_wst, ii_wst = self.BCEwst_v3_WH(cls_wst_gain, wst_ps, wst_t, epoch, epochs, is_train,
                                                                 cf_means, cf_move, fe_means, fe_vars, fe_move,
                                                                 measure_loss=measure)
                    if nl_wst != 0 and ii_wst != 0:
                        loss_wst = loss_wst / nl_wst
                        loss_wst = loss_wst / ii_wst
                        lwst += loss_wst
                    else:
                        lwst = lwst + 1e-5
                # t_wst_e = torch_utils.time_synchronized() - t_wst_s
                # print("\n t_wst用时 ===> ", t_wst_e)


        if self.autobalance:
            self.balance = [x / self.balance[self.ssi] for x in self.balance]
        lbox *= self.hyp['box']
        lobj *= self.hyp['obj']
        lcls *= self.hyp['cls']
        bs = tobj.shape[0]  # batch size


        ''' ========================= Wst 对比学习 ===================== '''
        if nt > 1:
            if epoch <= 500:
                lwst *= self.hyp['wst'] * self.hyp['wst_pw'] * wst_gain_1
                # print('if nt > 1: ===> lwst = ', lwst)
            else:
                lwst *= self.hyp['wst'] * self.hyp['wst_pw'] * wst_gain_2


        # loss = lbox + lobj + lcls
        # return loss * bs, torch.cat((lbox, lobj, lcls, loss)).detach()
        ''' ========================= Wst 对比学习 ===================== '''
        loss = lbox + lobj + lcls + lwst
        return loss * bs, torch.cat((lbox, lobj, lcls, lwst, loss)).detach()


    def build_targets(self, p, targets):
        # Build targets for compute_loss(), input targets(image,class,x,y,w,h)
        na, nt = self.na, targets.shape[0]  # number of anchors, targets
        tcls, tbox, indices, anch = [], [], [], []
        gain = torch.ones(7, device=targets.device)  # normalized to gridspace gain
        ai = torch.arange(na, device=targets.device).float().view(na, 1).repeat(1, nt)  # same as .repeat_interleave(nt)
        targets = torch.cat((targets.repeat(na, 1, 1), ai[:, :, None]), 2)  # append anchor indices

        g = 0.5  # bias
        off = torch.tensor([[0, 0],
                            [1, 0], [0, 1], [-1, 0], [0, -1],  # j,k,l,m
                            # [1, 1], [1, -1], [-1, 1], [-1, -1],  # jk,jm,lk,lm
                            ], device=targets.device).float() * g  # offsets

        for i in range(self.nl):
            anchors = self.anchors[i]
            gain[2:6] = torch.tensor(p[i].shape)[[3, 2, 3, 2]]  # xyxy gain

            # Match targets to anchors
            t = targets * gain
            if nt:
                # Matches
                r = t[:, :, 4:6] / anchors[:, None]  # wh ratio
                j = torch.max(r, 1. / r).max(2)[0] < self.hyp['anchor_t']  # compare
                # j = wh_iou(anchors, t[:, 4:6]) > model.hyp['iou_t']  # iou(3,n)=wh_iou(anchors(3,2), gwh(n,2))
                t = t[j]  # filter

                # Offsets
                gxy = t[:, 2:4]  # grid xy
                gxi = gain[[2, 3]] - gxy  # inverse
                j, k = ((gxy % 1. < g) & (gxy > 1.)).T
                l, m = ((gxi % 1. < g) & (gxi > 1.)).T
                j = torch.stack((torch.ones_like(j), j, k, l, m))
                t = t.repeat((5, 1, 1))[j]
                offsets = (torch.zeros_like(gxy)[None] + off[:, None])[j]
            else:
                t = targets[0]
                offsets = 0

            # Define
            b, c = t[:, :2].long().T  # image, class
            gxy = t[:, 2:4]  # grid xy
            gwh = t[:, 4:6]  # grid wh
            gij = (gxy - offsets).long()
            gi, gj = gij.T  # grid xy indices

            # Append
            a = t[:, 6].long()  # anchor indices
            indices.append((b, a, gj.clamp_(0, gain[3] - 1), gi.clamp_(0, gain[2] - 1)))  # image, anchor, grid indices
            tbox.append(torch.cat((gxy - gij, gwh), 1))  # box
            anch.append(anchors[a])  # anchors
            tcls.append(c)  # class

        return tcls, tbox, indices, anch
