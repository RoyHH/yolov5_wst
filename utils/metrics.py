# Model validation metrics

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn

from . import general

from Module_Store import *
# BN_WST_module = 'None'
# measure = 'Cosine'
# '''
# 无所谓 WST Head 是否单独，包含：L1, L2, Cosine
# 说明：
#     当 loss_module_name = 'v1_cosdis' 或 'v3_cosdis' 时，measure随意，因为不起作用
# '''


def fitness(x):
    # Model fitness as a weighted combination of metrics
    w = [0.0, 0.0, 0.1, 0.9]  # weights for [P, R, mAP@0.5, mAP@0.5:0.95]
    return (x[:, :4] * w).sum(1)


def ap_per_class(tp, conf, pred_cls, target_cls, plot=False, save_dir='.', names=()):
    """ Compute the average precision, given the recall and precision curves.
    Source: https://github.com/rafaelpadilla/Object-Detection-Metrics.
    # Arguments
        tp:  True positives (nparray, nx1 or nx10).
        conf:  Objectness value from 0-1 (nparray).
        pred_cls:  Predicted object classes (nparray).
        target_cls:  True object classes (nparray).
        plot:  Plot precision-recall curve at mAP@0.5
        save_dir:  Plot save directory
    # Returns
        The average precision as computed in py-faster-rcnn.
    """

    # Sort by objectness
    i = np.argsort(-conf)
    tp, conf, pred_cls = tp[i], conf[i], pred_cls[i]

    # Find unique classes
    unique_classes = np.unique(target_cls)
    nc = unique_classes.shape[0]  # number of classes, number of detections

    # Create Precision-Recall curve and compute AP for each class
    px, py = np.linspace(0, 1, 1000), []  # for plotting
    ap, p, r = np.zeros((nc, tp.shape[1])), np.zeros((nc, 1000)), np.zeros((nc, 1000))
    for ci, c in enumerate(unique_classes):
        i = pred_cls == c
        n_l = (target_cls == c).sum()  # number of labels
        n_p = i.sum()  # number of predictions

        if n_p == 0 or n_l == 0:
            continue
        else:
            # Accumulate FPs and TPs
            fpc = (1 - tp[i]).cumsum(0)
            tpc = tp[i].cumsum(0)

            # Recall
            recall = tpc / (n_l + 1e-16)  # recall curve
            r[ci] = np.interp(-px, -conf[i], recall[:, 0], left=0)  # negative x, xp because xp decreases

            # Precision
            precision = tpc / (tpc + fpc)  # precision curve
            p[ci] = np.interp(-px, -conf[i], precision[:, 0], left=1)  # p at pr_score

            # AP from recall-precision curve
            for j in range(tp.shape[1]):
                ap[ci, j], mpre, mrec = compute_ap(recall[:, j], precision[:, j])
                if plot and j == 0:
                    py.append(np.interp(px, mrec, mpre))  # precision at mAP@0.5

    # Compute F1 (harmonic mean of precision and recall)
    f1 = 2 * p * r / (p + r + 1e-16)
    if plot:
        plot_pr_curve(px, py, ap, Path(save_dir) / 'PR_curve.png', names)
        plot_mc_curve(px, f1, Path(save_dir) / 'F1_curve.png', names, ylabel='F1')
        plot_mc_curve(px, p, Path(save_dir) / 'P_curve.png', names, ylabel='Precision')
        plot_mc_curve(px, r, Path(save_dir) / 'R_curve.png', names, ylabel='Recall')

    i = f1.mean(0).argmax()  # max F1 index
    return p[:, i], r[:, i], ap, f1[:, i], unique_classes.astype('int32')


def compute_ap(recall, precision):
    """ Compute the average precision, given the recall and precision curves
    # Arguments
        recall:    The recall curve (list)
        precision: The precision curve (list)
    # Returns
        Average precision, precision curve, recall curve
    """

    # Append sentinel values to beginning and end
    mrec = np.concatenate(([0.], recall, [recall[-1] + 0.01]))
    mpre = np.concatenate(([1.], precision, [0.]))

    # Compute the precision envelope
    mpre = np.flip(np.maximum.accumulate(np.flip(mpre)))

    # Integrate area under curve
    method = 'interp'  # methods: 'continuous', 'interp'
    if method == 'interp':
        x = np.linspace(0, 1, 101)  # 101-point interp (COCO)
        ap = np.trapz(np.interp(x, mrec, mpre), x)  # integrate
    else:  # 'continuous'
        i = np.where(mrec[1:] != mrec[:-1])[0]  # points where x axis (recall) changes
        ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])  # area under curve

    return ap, mpre, mrec


class ConfusionMatrix:
    # Updated version of https://github.com/kaanakan/object_detection_confusion_matrix
    def __init__(self, nc, conf=0.25, iou_thres=0.45):
        self.matrix = np.zeros((nc + 1, nc + 1))
        self.nc = nc  # number of classes
        self.conf = conf
        self.iou_thres = iou_thres

    def process_batch(self, detections, labels):
        """
        Return intersection-over-union (Jaccard index) of boxes.
        Both sets of boxes are expected to be in (x1, y1, x2, y2) format.
        Arguments:
            detections (Array[N, 6]), x1, y1, x2, y2, conf, class
            labels (Array[M, 5]), class, x1, y1, x2, y2
        Returns:
            None, updates confusion matrix accordingly
        """
        detections = detections[detections[:, 4] > self.conf]
        gt_classes = labels[:, 0].int()
        detection_classes = detections[:, 5].int()
        iou = general.box_iou(labels[:, 1:], detections[:, :4])

        x = torch.where(iou > self.iou_thres)
        if x[0].shape[0]:
            matches = torch.cat((torch.stack(x, 1), iou[x[0], x[1]][:, None]), 1).cpu().numpy()
            if x[0].shape[0] > 1:
                matches = matches[matches[:, 2].argsort()[::-1]]
                matches = matches[np.unique(matches[:, 1], return_index=True)[1]]
                matches = matches[matches[:, 2].argsort()[::-1]]
                matches = matches[np.unique(matches[:, 0], return_index=True)[1]]
        else:
            matches = np.zeros((0, 3))

        n = matches.shape[0] > 0
        m0, m1, _ = matches.transpose().astype(np.int16)
        for i, gc in enumerate(gt_classes):
            j = m0 == i
            if n and sum(j) == 1:
                self.matrix[detection_classes[m1[j]], gc] += 1  # correct
            else:
                self.matrix[self.nc, gc] += 1  # background FP

        if n:
            for i, dc in enumerate(detection_classes):
                if not any(m1 == i):
                    self.matrix[dc, self.nc] += 1  # background FN

    def matrix(self):
        return self.matrix

    def plot(self, save_dir='', names=()):
        try:
            import seaborn as sn

            array = self.matrix / (self.matrix.sum(0).reshape(1, self.nc + 1) + 1E-6)  # normalize
            array[array < 0.005] = np.nan  # don't annotate (would appear as 0.00)

            fig = plt.figure(figsize=(12, 9), tight_layout=True)
            sn.set(font_scale=1.0 if self.nc < 50 else 0.8)  # for label size
            labels = (0 < len(names) < 99) and len(names) == self.nc  # apply names to ticklabels
            sn.heatmap(array, annot=self.nc < 30, annot_kws={"size": 8}, cmap='Blues', fmt='.2f', square=True,
                       xticklabels=names + ['background FP'] if labels else "auto",
                       yticklabels=names + ['background FN'] if labels else "auto").set_facecolor((1, 1, 1))
            fig.axes[0].set_xlabel('True')
            fig.axes[0].set_ylabel('Predicted')
            fig.savefig(Path(save_dir) / 'confusion_matrix.png', dpi=250)
        except Exception as e:
            pass

    def print(self):
        for i in range(self.nc + 1):
            print(' '.join(map(str, self.matrix[i])))


def is_parallel(model):
    # Returns True if model is of type DP or DDP
    return type(model) in (nn.parallel.DataParallel, nn.parallel.DistributedDataParallel)


def compute_cosine_L1(ps_1, ps_2):
    '''～～～～～～～～～～～～～～～～～ Cosine 版本 ～～～～～～～～～～～～～～～～～'''
    wst_d_a = ps_1.unsqueeze(dim=1)
    wst_d_b = ps_2.unsqueeze(dim=0)
    wst_d_temp = wst_d_a * wst_d_b
    wst_d_temp = wst_d_temp.sum(dim=2)

    wst_l1_a = torch.norm(ps_1, dim=1, p=1)
    wst_l1_a = wst_l1_a.unsqueeze(dim=1)
    wst_l1_b = torch.norm(ps_2, dim=1, p=1)
    wst_l1_b = wst_l1_b.unsqueeze(dim=0)
    wst_l1_temp = wst_l1_a * wst_l1_b

    wst_temp = wst_d_temp / wst_l1_temp
    temp_wst_temp = torch.max(wst_temp)
    '''～～～～～～～～～～～～～～～～～ Cosine 版本 -- end ～～～～～～～～～～～～～～～～～'''

    if temp_wst_temp > 1.0:
        print("\n\033[1;33m temp_wst_temp ===> ", temp_wst_temp)
        print("wst_d_temp ===> ", wst_d_temp)
        print("wst_l1_temp ===> ", wst_l1_temp)
        print("\033[0m")

    return wst_temp
# ===> (-1, 1)
def compute_cosine_L2(ps_1, ps_2):
    '''～～～～～～～～～～～～～～～～～ Cosine 版本 ～～～～～～～～～～～～～～～～～'''
    wst_d_a = ps_1.unsqueeze(dim=1)
    wst_d_b = ps_2.unsqueeze(dim=0)
    wst_d_temp = wst_d_a * wst_d_b
    wst_d_temp = wst_d_temp.sum(dim=2)

    wst_l2_a = torch.norm(ps_1, dim=1, p=2)
    wst_l2_a = wst_l2_a.unsqueeze(dim=1)
    wst_l2_b = torch.norm(ps_2, dim=1, p=2)
    wst_l2_b = wst_l2_b.unsqueeze(dim=0)
    wst_l2_temp = wst_l2_a * wst_l2_b

    wst_temp = wst_d_temp / wst_l2_temp
    wst_temp = wst_temp - 0.00001
    temp_wst_temp = torch.max(wst_temp)
    '''～～～～～～～～～～～～～～～～～ Cosine 版本 -- end ～～～～～～～～～～～～～～～～～'''

    if temp_wst_temp > 1.0:
        print("\n\033[1;33m temp_wst_temp ===> ", temp_wst_temp)
        print("wst_d_temp ===> ", wst_d_temp)
        print("wst_l2_temp ===> ", wst_l2_temp)
        print("\033[0m")

    return wst_temp
class A(object):
    temp_self = 'temp_objectt'
def build_targets(p, targets, model):
    # Build targets for compute_loss(), input targets(image,class,x,y,w,h)
    some_det = model.module.model[-1] if is_parallel(model) else model.model[-1]  # number of anchors
    det_self = A()
    for k in 'na', 'nc', 'nl', 'anchors':
        setattr(det_self, k, getattr(some_det, k))
    na = det_self.na
    nt = targets.shape[0]  # targets
    tcls, tbox, indices, anch = [], [], [], []
    gain = torch.ones(7, device=targets.device)  # normalized to gridspace gain
    ai = torch.arange(na, device=targets.device).float().view(na, 1).repeat(1, nt)  # same as .repeat_interleave(nt)
    targets = torch.cat((targets.repeat(na, 1, 1), ai[:, :, None]), 2)  # append anchor indices

    g = 0.5  # bias
    off = torch.tensor([[0, 0],
                        [1, 0], [0, 1], [-1, 0], [0, -1],  # j,k,l,m
                        # [1, 1], [1, -1], [-1, 1], [-1, -1],  # jk,jm,lk,lm
                        ], device=targets.device).float() * g  # offsets

    for i in range(det_self.nl):
        anchors = det_self.anchors[i]
        gain[2:6] = torch.tensor(p[i].shape)[[3, 2, 3, 2]]  # xyxy gain

        # Match targets to anchors
        t = targets * gain
        if nt:
            # Matches
            r = t[:, :, 4:6] / anchors[:, None]  # wh ratio
            j = torch.max(r, 1. / r).max(2)[0] < model.hyp['anchor_t']  # compare
            # j = wh_iou(anchors, t[:, 4:6]) > model.hyp['iou_t']  # iou(3,n)=wh_iou(anchors(3,2), gwh(n,2))
            t = t[j]  # filter

            # Offsets
            gxy = t[:, 2:4]  # grid xy
            gxi = gain[[2, 3]] - gxy  # inverse
            j, k = ((gxy % 1. < g) & (gxy > 1.)).T
            l, m = ((gxi % 1. < g) & (gxi > 1.)).T
            j = torch.stack((torch.ones_like(j), j, k, l, m))
            t = t.repeat((5, 1, 1))[j]
            offsets = (torch.zeros_like(gxy)[None] + off[:, None])[j]
        else:
            t = targets[0]
            offsets = 0

        # Define
        b, c = t[:, :2].long().T  # image, class
        gxy = t[:, 2:4]  # grid xy
        gwh = t[:, 4:6]  # grid wh
        gij = (gxy - offsets).long()
        gi, gj = gij.T  # grid xy indices

        # Append
        a = t[:, 6].long()  # anchor indices
        indices.append((b, a, gj.clamp_(0, gain[3] - 1), gi.clamp_(0, gain[2] - 1)))  # image, anchor, grid indices
        tbox.append(torch.cat((gxy - gij, gwh), 1))  # box
        anch.append(anchors[a])  # anchors
        tcls.append(c)  # class

    return tcls, tbox, indices, anch
def smooth_BCE(eps=0.1):  # https://github.com/ultralytics/yolov3/issues/238#issuecomment-598028441
    # return positive, negative label smoothing BCE targets
    return 1.0 - 0.5 * eps, 0.5 * eps
def test_bn_linear(x, x_max, x_min):
    x = x - x_max - 0.001
    x = torch.div(x, (0.002 + (x_max + 0.001 - x_min)))    # (-1, 0)
    return x

'''类别均值向量全局差值评价模块 ——WST Head'''
def batch_norm_wst(x, gamma, beta, moving_means, moving_vars, moving_momentum, is_train=True, bn_dim=0):
    eps = 1e-5
    x_mean = torch.mean(x, dim=bn_dim)
    x_var = torch.sqrt(torch.mean((x - x_mean) ** 2, dim=bn_dim))
    if is_train == True:
        moving_means = moving_momentum * moving_means + (1. - moving_momentum) * x_mean
        moving_vars = moving_momentum * moving_vars + (1. - moving_momentum) * x_var
        x_hat = (x - x_mean) / (x_var + eps)
        return gamma * x_hat + beta, moving_means.detach(), moving_vars.detach()
    else:
        x_mean = (1. - moving_momentum) * moving_means + moving_momentum * x_mean
        x_var = (1. - moving_momentum) * moving_vars + moving_momentum * x_var
        x_hat = (x - x_mean) / (x_var + eps)
        return gamma * x_hat + beta, moving_means, moving_vars
def cf_wst(x, c_means, moving_momentum, is_train=True, c_dim=0):
    x_mean = torch.mean(x, dim=c_dim)
    c_means = moving_momentum * c_means + (1. - moving_momentum) * x_mean
    if is_train == True:
        return c_means, c_means.detach()
    else:
        return c_means, c_means
def wst_v3_test_WH(p, targets, model, epoch, epochs, cf_means, cf_move, fe_means, fe_vars, fe_move_test,
                   wst_check_w_01, wst_check_w_10, wst_check_0, wst_check_1, wst_check_w_01_ij, wst_check_w_10_ij):
    tcls, tbox, indices, anchors = build_targets(p, targets, model)  # targets
    # per output
    nt = 0  # targets
    cp, cn = smooth_BCE(eps=0.0)
    dy0_pre_right = 0
    dy1_pre_right = 0
    dy0_pre_wrong = 0
    dy1_pre_wrong = 0
    wst_check_w_01 = torch.from_numpy(wst_check_w_01).to(targets.device)
    wst_check_w_10 = torch.from_numpy(wst_check_w_10).to(targets.device)
    wst_check_w_01_ij = torch.from_numpy(wst_check_w_01_ij).to(targets.device)
    wst_check_w_10_ij = torch.from_numpy(wst_check_w_10_ij).to(targets.device)
    wst_check_0 = torch.from_numpy(wst_check_0).to(targets.device)
    wst_check_1 = torch.from_numpy(wst_check_1).to(targets.device)
    temp_rr = 0.0
    for i, pi in enumerate(p):  # layer index, layer predictions
        '''================================ 每个anchor的loss计算 ====================================='''
        b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx #image表示的是batch-size中第几张图在当前yolo layer上有target
        # print("\n image-b, anchor-a, gridy-gj, gridx-gi =\n[%s,\n %s,\n %s,\n %s] " % (b, a, gj, gi))
        nb = b.shape[0]  # number of targets
        if nb:
            nt += nb  # cumulative targets
            ps = pi[b, a, gj, gi]  # prediction subset corresponding to targets

            # Class
            if model.nc > 1:  # cls loss (only if multiple classes)
                t = torch.full_like(ps[:, 5:], cn)  # targets
                t[range(nb), tcls[i]] = cp

                '''========================== Meta Learning ================================='''
                if nb > 1:
                    '''====================== ps中的feature vecs做预处理 ======================'''
                    _, k_t_id = torch.max(t, dim=1)  # find the class of every feature vec
                    old_ps = ps.detach()
                    # old_ps[:, 5:] = old_ps[:, 5:].sigmoid()

                    '''============ 特征向量预处理 BN操作 ============'''
                    gamma_bn = torch.ones(1, device='cuda:0')
                    beta_bn = torch.zeros(1, device='cuda:0')
                    # 各特征维度做BN，在单一特征维度上保持正态分布
                    m_m = torch.tensor((10.0 * epoch / epochs))
                    m_mfe = torch.sigmoid(m_m) * fe_move_test
                    if BN_WST_module == 'Done':
                        old_ps[:, 5:], fe_means[epoch, :], fe_vars[epoch, :] = \
                            batch_norm_wst(old_ps[:, 5:], gamma_bn, beta_bn, fe_means[epoch, :],
                                                  fe_vars[epoch, :], m_mfe, is_train=False, bn_dim=0)

                    new_ps = old_ps[:, 5:]
                    new_ps = torch.cat((k_t_id[:, None], new_ps), 1)  # feature vec里在第一个元素上加入类别标签

                    '''====================== 基于类别，统计各类feature vecs的均值向量 ======================'''
                    '''
                    ct：统计当前ps序列中所有出现的类别
                    '''
                    ct_idx = t.sum(dim=0)
                    ct_idx = ct_idx.cpu().numpy()
                    ct = np.argwhere(ct_idx != 0)
                    ct = torch.from_numpy(ct)
                    ct = ct.to(new_ps.device)
                    # print("\n ct \n", ct)

                    '''
                    idx_ci：当前ps序列中属于i类的行索引
                    ps_ci：当前ps序列中属于i类的所以feature vecs的集合
                    m_ps_ci：当前ps序列中属于i类的所以feature vecs的均值向量
                    '''
                    new_ps_n = new_ps.detach().cpu().numpy()  # 便于操作

                    m_mcf = torch.sigmoid(m_m) * cf_move
                    names = locals()  # 便于动态命名，使其变量名能与类别标签对应上
                    for ci in range(model.nc):
                        names['idx_c%s' % ci] = np.argwhere(new_ps_n[:, 0] == ci)
                        if names['idx_c%s' % ci].size != 0:
                            names['idx_c%s' % ci] = torch.from_numpy(names['idx_c%s' % ci]).to(new_ps.device)
                            names['ps_c%s' % ci] = new_ps[names['idx_c%s' % ci], 1:]
                            names['ps_c%s' % ci] = names['ps_c%s' % ci].squeeze(1)
                            # names['m_ps_c%s' % ci] = torch.mean(names['ps_c%s' % ci], dim=0)    # 簇心
                            names['m_ps_c%s' % ci], cf_means[epoch, ci, :] = \
                                cf_wst(names['ps_c%s' % ci], cf_means[epoch, ci, :], m_mcf, is_train=False, c_dim=0)

                    '''m_ps_cs：当前ps序列中所有出现的类别的均值向量集合'''
                    for ci in range(ct.shape[0]):
                        temp_ci = int(ct[ci, 0])
                        if ci == 0:
                            m_ps_cs = names['m_ps_c%s' % temp_ci].unsqueeze(0)
                            m_ps_ci_wcl = names['m_ps_c%s' % temp_ci].unsqueeze(0)
                            m_ps_ci_wcl = torch.cat((ct[ci].unsqueeze(0), m_ps_ci_wcl), 1)
                            m_ps_cs_wcl = m_ps_ci_wcl
                        else:
                            m_ps_cs = torch.cat((m_ps_cs, names['m_ps_c%s' % temp_ci].unsqueeze(0)), 0)
                            m_ps_ci_wcl = names['m_ps_c%s' % temp_ci].unsqueeze(0)
                            m_ps_ci_wcl = torch.cat((ct[ci].unsqueeze(0), m_ps_ci_wcl), 1)
                            m_ps_cs_wcl = torch.cat((m_ps_cs_wcl, m_ps_ci_wcl), 0)

                    '''====================== 计算差值 ======================'''
                    '''
                    rd_cs_ps：将所有rd_ci按照类别号依次拼接为ps.shape[0]大小的集合
                    idx_ct_ps：rd_cs_ps 中元素对应的feature vecs在ps中的行索引
                    '''
                    for ci in range(ct.shape[0]):
                        temp_ci = int(ct[ci, 0])
                        if measure == 'L1':
                            names['rd_c%s' % temp_ci] = torch.sub(names['ps_c%s' % temp_ci],
                                                                  names['m_ps_c%s' % temp_ci]).abs().sum(dim=1)
                        elif measure == 'L2':
                            temp_aaa_subdis = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci])
                            names['rd_c%s' % temp_ci] = torch.norm(temp_aaa_subdis, dim=1, p=2).unsqueeze(dim=1)
                        elif measure == 'Cosine':
                            temp_aaa = compute_cosine_L2(names['ps_c%s' % temp_ci],
                                                         names['m_ps_c%s' % temp_ci].unsqueeze(dim=0))    # (-1, 1)
                            names['rd_c%s' % temp_ci] = (1.0 - temp_aaa).squeeze(dim=1)    # (0, 2)

                            if torch.max(temp_aaa) > 1.0:
                                print("\n\033[1;33m training: ")
                                print("temp_aaa ===> ", temp_aaa)
                                print("temp_aaa ===> %.20f" % torch.max(temp_aaa))
                                print("\033[0m")


                        if ci == 0:
                            rd_cs_ps = names['rd_c%s' % temp_ci]
                            temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                            idx_ct_ps = temp_idx
                        else:
                            rd_cs_ps = torch.cat((rd_cs_ps, names['rd_c%s' % temp_ci]), 0)
                            temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                            idx_ct_ps = torch.cat((idx_ct_ps, temp_idx), 0)

                    '''
                    rd_all：ps中所有feature vecs与ps中所有出现过的类别的均值的差值（L1）
                    '''
                    if measure == 'L1':
                        rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=1)
                    elif measure == 'L2':
                        rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=2)
                    elif measure == 'Cosine':
                        temp_bbb = compute_cosine_L2(ps[:, 5:], m_ps_cs)    # (-1, 1)
                        rd_all = 1.0 - temp_bbb    # (0, 2)


                    '''
                    rd_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将ps重组
                    rt_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将t重组
                    '''
                    idx_ct_ps = idx_ct_ps.to(torch.int64)
                    rd_all_ct_ps = torch.index_select(rd_all, 0, idx_ct_ps)
                    rt_all_ct_ps = torch.index_select(t, 0, idx_ct_ps)

                    '''
                    rd_cs_ps_d1：重组的ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1）
                    rd_cs_ps_d0：重组的ps中属于i类别的feature vecs与i类别的均值的差值（L1）
                    '''
                    rd_cs_ps_d0 = rd_cs_ps[None, :].t()    # (0, 2)
                    temp_0_min = torch.min(rd_cs_ps_d0)
                    temp_0_max = torch.max(rd_cs_ps_d0)
                    rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0)    # (-2, 2)


                    # if temp_0_min < 0.0:
                    #     print("testing \n rd_cs_ps ===> ", rd_cs_ps)


# need change because ct maybe equel 1
                    if ct.shape[0] != 1:
                        temp_rr += 1.0
                        ''' 
                        rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 
                        '''
                        for ci in range(ct.shape[0]):
                            temp_ci = int(ct[ci, 0])
                            if ci == 0:
                                ci_idx = ci
                            else:
                                ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
                            names['rd_cs_ps_d1_c%s' % temp_ci] = rd_cs_ps_d1[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
                            names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
                            names['rd_cs_ps_d1_c%s' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci][:, torch.arange(
                                names['rd_cs_ps_d1_c%s' % temp_ci].size(1)) != ci]
                            names['dy1_c%s_ct' % temp_ci] = ct[torch.arange(ct.size(0)) != ci]
                            # print("\n names['dy1_c%s_ct] = %s \n" % (temp_ci, names['dy1_c%s_ct' % temp_ci]))
                            if ci == 0:
                                temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci])
                                temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci])
                            else:
                                temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.max(
                                    names['rd_cs_ps_d1_c%s' % temp_ci]) > temp_1_max else temp_1_max
                                temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.min(
                                    names['rd_cs_ps_d1_c%s' % temp_ci]) < temp_1_min else temp_1_min

                        ''' 
                        rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid 
                        '''
                        for ci in range(ct.shape[0]):
                            temp_ci = int(ct[ci, 0])
                            if names['rd_cs_ps_d1_c%s' % temp_ci].size != 0:
                                if measure == 'L1' or measure == 'L2':
                                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = test_bn_linear(names['rd_cs_ps_d1_c%s' % temp_ci], temp_1_max, temp_1_min)   # (-1, 0)
                                    # names['rd_cs_ps_d1_c%s_p' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci] - temp_1_max - 0.001
                                    # names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d1_c%s_p' % temp_ci],
                                    #                                                  (0.002 + (temp_1_max + 0.001 - temp_1_min)))   # (-1, 0)

                                    # temp_1_mid = torch.div(10, (0.002 + (temp_1_max + 0.001 - temp_1_min)))
                                    temp_1_mid = 0.5
                                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + temp_1_mid)   # (-6, 6)

                                elif measure == 'Cosine':
                                    names['rd_cs_ps_d1_c%s_p' % temp_ci] = 3.0 * names['rd_cs_ps_d1_c%s' % temp_ci]   # (-6, 6)

                                names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d1_c%s_p' % temp_ci])


                                if measure == 'L1' or measure == 'L2':
                                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = test_bn_linear(names['rd_cs_ps_d0_c%s' % temp_ci], temp_0_max, temp_0_min)   # (-1, 0)
                                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] - temp_0_max - 0.001
                                    # names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci],
                                    #                                                  (0.002 + (temp_0_max + 0.001 - temp_0_min)))   # (-1, 0)

                                    # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
                                    temp_0_mid = 0.5
                                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] + temp_0_mid)   # (-6, 6)

                                elif measure == 'Cosine':
                                    names['rd_cs_ps_d0_c%s_p' % temp_ci] = 6.0 * (names['rd_cs_ps_d0_c%s' % temp_ci] - 1.0)   # (-6, 6)

                                names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])


                                names['dy0_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d0_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2
                                wst_check_0[temp_ci, temp_ci] += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0]

                                dy0_pre_wrong += names['dy0_pre_c%s_p' % temp_ci].sum()
                                dy0_pre_right += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0] - names['dy0_pre_c%s_p' % temp_ci].sum()

                                names['dy1_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d1_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2

                                dy1_pre_right += names['dy1_pre_c%s_p' % temp_ci].sum()
                                dy1_pre_wrong += names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0] * \
                                                 names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[1] - names['dy1_pre_c%s_p' % temp_ci].sum()

                                for cj in range(names['dy1_c%s_ct' % temp_ci].shape[1]):
                                    temp_cj = int(names['dy1_c%s_ct' % temp_ci][cj, 0])
                                    wst_check_1[temp_ci, temp_cj] += names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0]

                                    for i in range(names['dy0_pre_c%s_p' % temp_ci].shape[0]):
                                        if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 1.0:
                                            wst_check_w_01[temp_ci, temp_ci] += 1

                                            if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
                                                wst_check_w_01_ij[temp_ci, temp_cj] += 1

                                        if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
                                            wst_check_w_10[temp_ci, temp_cj] += 1

                                            if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 0.0:
                                                wst_check_w_10_ij[temp_cj, temp_ci] += 1

                    else:
                        ''' 
                        rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 
                        '''
                        for ci in range(ct.shape[0]):
                            temp_ci = int(ct[ci, 0])
                            if ci == 0:
                                ci_idx = ci
                            else:
                                ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
                            names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]

                        ''' 
                        rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid 
                        '''
                        for ci in range(ct.shape[0]):
                            temp_ci = int(ct[ci, 0])
                            if measure == 'L1' or measure == 'L2':
                                names['rd_cs_ps_d0_c%s_p' % temp_ci] = test_bn_linear(names['rd_cs_ps_d0_c%s' % temp_ci], temp_0_max, temp_0_min)   # (-1, 0)
                                # names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] - temp_0_max - 0.001
                                # names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci],
                                #                                                  (0.002 + (temp_0_max + 0.001 - temp_0_min)))   # (-1, 0)

                                # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
                                temp_0_mid = 0.5
                                names['rd_cs_ps_d0_c%s_p' % temp_ci] = 12.0 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] + temp_0_mid)   # (-6, 6)

                            elif measure == 'Cosine':
                                names['rd_cs_ps_d0_c%s_p' % temp_ci] = 6.0 * (names['rd_cs_ps_d0_c%s' % temp_ci] - 1.0)   # (-6, 6)

                            names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])


                            names['dy0_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d0_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2
                            wst_check_0[temp_ci, temp_ci] += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0]

                            dy0_pre_wrong += names['dy0_pre_c%s_p' % temp_ci].sum()
                            dy0_pre_right += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0] - names['dy0_pre_c%s_p' % temp_ci].sum()

                            for i in range(names['dy0_pre_c%s_p' % temp_ci].shape[0]):
                                if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 1.0:
                                    wst_check_w_01[temp_ci, temp_ci] += 1

    m_ps_cs_wcl = m_ps_cs_wcl.cpu().numpy()

    wst_check_w_01 = wst_check_w_01.cpu().numpy()
    wst_check_w_10 = wst_check_w_10.cpu().numpy()
    wst_check_w_01_ij = wst_check_w_01_ij.cpu().numpy()
    wst_check_w_10_ij = wst_check_w_10_ij.cpu().numpy()
    wst_check_0 = wst_check_0.cpu().numpy()
    wst_check_1 = wst_check_1.cpu().numpy()

    if temp_rr == 0.0:
        dy0_pre_right = dy0_pre_right.cpu().numpy()
        dy0_pre_wrong = dy0_pre_wrong.cpu().numpy()
        dy1_pre_right = np.zeros_like(dy0_pre_right)
        dy1_pre_wrong = np.zeros_like(dy0_pre_wrong)
    else:
        dy0_pre_right = dy0_pre_right.cpu().numpy()
        dy1_pre_right = dy1_pre_right.cpu().numpy()
        dy0_pre_wrong = dy0_pre_wrong.cpu().numpy()
        dy1_pre_wrong = dy1_pre_wrong.cpu().numpy()

    # print("\n m_ps_cs_wcl \n", m_ps_cs_wcl)
    # print("\n wst_check_0 \n", wst_check_0)
    # print("\n wst_check_1 \n", wst_check_1)
    # print("\n dy0_pre_right \n", dy0_pre_right)
    # print("\n dy0_pre_wrong \n", dy0_pre_wrong)
    # print("\n dy1_pre_right \n", dy1_pre_right)
    # print("\n dy1_pre_wrong \n", dy1_pre_wrong)
    # print("\n wst_check_w_01 \n", wst_check_w_01)
    # print("\n wst_check_w_01_ij \n", wst_check_w_01_ij)
    # print("\n wst_check_w_10 \n", wst_check_w_10)
    # print("\n wst_check_w_10_ij \n", wst_check_w_10_ij)

    return wst_check_w_01, wst_check_w_10, wst_check_0, wst_check_1, \
           wst_check_w_01_ij, wst_check_w_10_ij, dy0_pre_right, dy0_pre_wrong, dy1_pre_right, dy1_pre_wrong

'''Acc评价模块 —— 包含“考虑类别正确与否” 和 “不考虑类别正确与否” 两种'''
def remove_diag_effect(tensor_in):
    diag = torch.diag(tensor_in)
    e_diag = torch.diag_embed(diag)
    tensor_in = tensor_in - e_diag
    return tensor_in
def statis_rw(numpy_in, r=None, w=None):
    aaa0 = np.argwhere(numpy_in == r)
    bbb0 = np.argwhere(numpy_in == w)
    pre_r = aaa0.shape[0] / 2
    pre_w = bbb0.shape[0] / 2
    return pre_r, pre_w
def statis_rw_c(numpy_in, r=None, w1=None, w2=None):
    aaa0c = np.argwhere(numpy_in == r)
    bbb0c = numpy_in <= w1
    ccc0c = numpy_in >= w2
    temp_bc0c = bbb0c & ccc0c
    temp_0c = np.argwhere(temp_bc0c == True)
    pre_r = aaa0c.shape[0] / 2
    pre_w = temp_0c.shape[0] / 2
    return pre_r, pre_w
def check_acc(p, targets, model, c0_pre_rsum, c0_pre_wsum, c1_pre_rsum, c1_pre_wsum,
              c0_pre_rsum_c, c0_pre_wsum_c, c1_pre_rsum_c, c1_pre_wsum_c):
    tcls, tbox, indices, anchors = build_targets(p, targets, model)  # targets
    # per output
    nt = 0  # targets
    cp, cn = smooth_BCE(eps=0.0)
    for i, pi in enumerate(p):  # layer index, layer predictions
        '''================================ 每个anchor的loss计算 ====================================='''
        b, a, gj, gi = indices[i]  # image, anchor, gridy, gridx #image表示的是batch-size中第几张图在当前yolo layer上有target
        # print("\n image-b, anchor-a, gridy-gj, gridx-gi =\n[%s,\n %s,\n %s,\n %s] " % (b, a, gj, gi))
        nb = b.shape[0]  # number of targets
        if nb:
            nt += nb  # cumulative targets
            ps = pi[b, a, gj, gi]  # prediction subset corresponding to targets

            # Class
            if model.nc > 1:  # cls loss (only if multiple classes)
                t = torch.full_like(ps[:, 5:], cn)  # targets
                t[range(nb), tcls[i]] = cp

                '''========================== Meta Learning ================================='''
                if nb > 1:
                    check_ps = ps[:, 5:].detach()
                    check_ps = check_ps.sigmoid()  # sigmoid
                    check_t = t.detach()
                    _, ct = torch.max(check_t, dim=1)
                    _, cps = torch.max(check_ps, dim=1)
                    ct = ct.to(float)
                    cps = cps.to(float)

                    '''
                    pre与label对比mask：
                    -4 —— 2个pre都与各自label不符
                    -1 —— 1个pre都与各自label不符
                    2  —— 2个pre都与各自label相符
                    '''
                    label_pre_check = ct - cps
                    ones_lpc = torch.ones_like(label_pre_check)
                    others_lpc = 0.0 - 2 * ones_lpc
                    label_pre_check = torch.where(label_pre_check == 0.0, ones_lpc, others_lpc)

                    temp_lpc = label_pre_check.unsqueeze(dim=1)
                    temp_lpc = temp_lpc.expand(temp_lpc.shape[0], temp_lpc.shape[0])
                    label_pre_check = label_pre_check + temp_lpc
                    # 去对角线元素影响
                    label_pre_check = remove_diag_effect(label_pre_check)
                    # print('label_pre_check = ', label_pre_check)

                    '''
                    基于label的对比mask：
                    0 —— 自身比较，即对角线，略
                    1 —— 通过label识别，2个label同类
                    10 —— 通过label识别，2个label不同类
                    '''
                    label_sd = torch.norm(check_t[:, None] - check_t, dim=2, p=1) / 2
                    ones_l = torch.ones_like(label_sd)
                    others_l = 10 * ones_l
                    label_sd = torch.where(label_sd > 0.0, others_l, ones_l)
                    # 去对角线元素影响
                    label_sd = remove_diag_effect(label_sd)
                    # print('label_sd = ', label_sd)

                    '''
                    基于pre的对比mask：
                    0 —— 自身比较，即对角线，略
                    1 —— 通过pre识别，2个pre同类
                    2 —— 通过pre识别，2个pre不同类
                    '''
                    temp_cps = cps.unsqueeze(1)
                    pred_sd = torch.norm(temp_cps[:, None] - temp_cps, dim=2, p=1)
                    ones_p = torch.ones_like(pred_sd)
                    others_p = 2 * ones_p
                    pred_sd = torch.where(pred_sd > 0.0, others_p, ones_p)
                    # 去对角线元素影响
                    pred_sd = remove_diag_effect(pred_sd)
                    # print('pred_sd = ', pred_sd)

                    sd_lp_check = label_sd * pred_sd
                    # print('sd_lp_check = ', sd_lp_check)
                    sd_lp_check_n = sd_lp_check.detach().cpu().numpy()
                    c0_pre_r, c0_pre_w = statis_rw(sd_lp_check_n, r=1.0, w=2.0)
                    c1_pre_r, c1_pre_w = statis_rw(sd_lp_check_n, r=20.0, w=10.0)
                    c0_pre_rsum += c0_pre_r
                    c0_pre_wsum += c0_pre_w
                    c1_pre_rsum += c1_pre_r
                    c1_pre_wsum += c1_pre_w
                    # print("\n c0_pre_r = %s\n c0_pre_w = %s\n c1_pre_r = %s\n c1_pre_w = %s"
                    #       % (c0_pre_r, c0_pre_w, c1_pre_r, c1_pre_w))

                    label_pre_check_n = label_pre_check.detach().cpu().numpy()
                    sta_lpc_check = label_pre_check_n * sd_lp_check_n
                    # print('sta_lpc_check = ', sta_lpc_check)
                    c0_pre_rc, c0_pre_wc = statis_rw_c(sta_lpc_check, r=2.0, w1=-1.0, w2=-8.0)
                    c1_pre_rc, c1_pre_wc = statis_rw_c(sta_lpc_check, r=40.0, w1=-10.0, w2=-80.0)
                    c0_pre_rsum_c += c0_pre_rc
                    c0_pre_wsum_c += c0_pre_wc
                    c1_pre_rsum_c += c1_pre_rc
                    c1_pre_wsum_c += c1_pre_wc
                    # print("\n c0_pre_rc = %s\n c0_pre_wc = %s\n c1_pre_rc = %s\n c1_pre_wc = %s"
                    #       % (c0_pre_rc, c0_pre_wc, c1_pre_rc, c1_pre_wc))

    return c0_pre_rsum, c0_pre_wsum, c1_pre_rsum, c1_pre_wsum, c0_pre_rsum_c, c0_pre_wsum_c, c1_pre_rsum_c, c1_pre_wsum_c


# Plots ----------------------------------------------------------------------------------------------------------------

def plot_pr_curve(px, py, ap, save_dir='pr_curve.png', names=()):
    # Precision-recall curve
    fig, ax = plt.subplots(1, 1, figsize=(9, 6), tight_layout=True)
    py = np.stack(py, axis=1)

    if 0 < len(names) < 21:  # display per-class legend if < 21 classes
        for i, y in enumerate(py.T):
            ax.plot(px, y, linewidth=1, label=f'{names[i]} {ap[i, 0]:.3f}')  # plot(recall, precision)
    else:
        ax.plot(px, py, linewidth=1, color='grey')  # plot(recall, precision)

    ax.plot(px, py.mean(1), linewidth=3, color='blue', label='all classes %.3f mAP@0.5' % ap[:, 0].mean())
    ax.set_xlabel('Recall')
    ax.set_ylabel('Precision')
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    plt.legend(bbox_to_anchor=(1.04, 1), loc="upper left")
    fig.savefig(Path(save_dir), dpi=250)


def plot_mc_curve(px, py, save_dir='mc_curve.png', names=(), xlabel='Confidence', ylabel='Metric'):
    # Metric-confidence curve
    fig, ax = plt.subplots(1, 1, figsize=(9, 6), tight_layout=True)

    if 0 < len(names) < 21:  # display per-class legend if < 21 classes
        for i, y in enumerate(py):
            ax.plot(px, y, linewidth=1, label=f'{names[i]}')  # plot(confidence, metric)
    else:
        ax.plot(px, py.T, linewidth=1, color='grey')  # plot(confidence, metric)

    y = py.mean(0)
    ax.plot(px, y, linewidth=3, color='blue', label=f'all classes {y.max():.2f} at {px[y.argmax()]:.3f}')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    plt.legend(bbox_to_anchor=(1.04, 1), loc="upper left")
    fig.savefig(Path(save_dir), dpi=250)
