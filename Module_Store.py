import os

file_cf_means = 'WST_cf_means'
wst_check_w_file = 'wst_check_w.txt'
wst_check_file = 'wst_check_w%.txt'
wst_check_wij_file = 'wst_check_wij.txt'

# wst_gain for compute loss
wst_gain_1 = 0.5
wst_gain_2 = 0.5

WST_module_name = 'Single'
'''
包含：Single, Same, None
说明：
    Single: WST Head 单独存在
    Same: WST Head 与 yolo Head 同一个
    None: 没有 WST 功能时，原 YOLO
'''

wst_loss = 'v3'
'''
无所谓 WST Head 是否单独，包含：v1, v3
说明：
    v1：关键点加权差值方法, 直接差值也在这里, loss_module_name = 'v1' 或 'v1_Cosdis'
    v3：类别均值向量全局差值方法, loss_module_name = 'v3' 或 'v3_Cosdis'
'''

loss_module_name = 'v3'
'''
当 WST Head 单独时，包含：v1, v1_cosdis, v3, v3_cosdis
说明：
    v1: L1, L2, Cosine of v1
    v1_cosdis: Cosdis of v1
    v3: L1, L2, Cosine of v3
    v3_cosdis: Cosdis of v3
当 WST Head 与 yolo Head 同一时，包含：v1, v3
说明：
    v1: L1, L2, Cosine of v1
    v3: L1, L2, Cosine of v3
'''

measure = 'Cosine'
'''
无所谓 WST Head 是否单独，包含：L1, L2, Cosine
说明：
    当 loss_module_name = 'v1_cosdis' 或 'v3_cosdis' 时，measure随意，因为不起作用
'''

BN_WST_module = 'None'
'''
包含：Done, None
说明：
    Done: 在做度量前 Feature maps 做 BN 操作
    None: 在做度量前 Feature maps 不做 BN 操作
'''

OHEM_module = 'None'
'''
包含：Done, None
说明：
    Done: 在做Loss前做 OHEM 难例硬筛选操作
    None: 在做Loss前不做 OHEM 难例硬筛选操作
'''

multi_task_module = 'Done'


